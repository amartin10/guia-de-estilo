# Atomic Design & Arquitectura Hexagonal con VueJS

**TODO:** Establecer si utilizar un estilo de programación más declarativa y siguiendo los principios de la programación funcional. Queda muy guay para debug, testing y reutilizar funcionalidad :D.

## Contexto Shared:
```
/infrastructure
    /components
        /atoms                     
            Button.vue
            SelectBox.vue
            Input.vue
            TableRow.vue
        /molecules
            SearchForm.vue      (Input.vue + Button.vue)
            Table.vue           (Multiples TableRow.vue)
            DataSet.vue         (??)
```

- Los átomos sólo deben incluirse en *Shared*. Si es necesario crear un átomo fuera de éste contexto, quizás hay que pensar en generalizar alguno ya creado en _Shared_.
- Los átomos deben ser en la medida de lo posible [componentes funcionales](https://es.vuejs.org/v2/guide/render-function.html#Componentes-Funcionales).
- Utilizar anotación `@Emit` para lanzar eventos de hijo al padre. No pasarle al hijo una `@Prop` con la función del padre a ejecutar.
- La funcionalidad de 'filtrar' los datos (por ejemplo, solo renderizar el 'nombre' de un `SelectBox` debe ser una lógica del propio componente `SelectBox`).
- **TODO**: Revisar el wrapper `testingLibraryWrapper` para testear la emisión de eventos.
- **TODO**: Fijar standard para el nombre de las _props_ y _emitters_ 


## Otros contextos
Dentro de otro contexto, pensemos en el contexto *Spa*, podemos querer tener una tabla que:

1) Renderize la lista de *Spa*
2) Filtre por *Brand*
3) Filtre por nombre.

```
/infrastructure
    /components
        /molecules
            SpaTableToolBar.vue     (SelectBox.vue + SearchForm.vue)
        /organisms
            SpaTable.vue     (Table.vue + SpaTableToolBar)
    /containers
            SpaTableContainer.vue       (SpaTable.vue)
```
- Cada _container_ se corresponde con un único organismo.
- Los _containers_ son los únicos que pueden tener inyectado un _useCase_.
- Los _containers_ deben ser componentes śolo con 'lógica de datos' (consumir los datos del useCase y manipularlos si es necesario) y deben estar formado por un sólo archivo .ts con una función de renderizado como se muestra en [componentes funcionales](https://es.vuejs.org/v2/guide/render-function.html#Componentes-Funcionales) que renderiza su organismo correspondiente.

**Nota**: Esta separación entre _components_ y _containers_ puede parecer tediosa pero puede facilitar enormemente el testing **(Nos podemos olvidar de los _lifecycle methods_ en los organismos!!)**. 

Esta idea está sacada de este [artículo](https://medium.com/@dan_abramov/smart-and-dumb-components-7ca2f9a7c7d0) de Dan Abramov :D 